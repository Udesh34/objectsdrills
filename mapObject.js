function mapObject(obj,cb){
    for (const [k,v] of Object.entries(obj)){
        obj[k]=cb(v);
          
    }
    return obj

}
export {mapObject}
