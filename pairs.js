function pairs(obj){
    let ans=[];
    for (let k in obj){
        ans.push([k,obj[k]]);
    }
    return ans;
}
export {pairs}
