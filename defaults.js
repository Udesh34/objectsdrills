function defaults(obj,defaultProps){
  let keysArr=[];
  for(let k in defaultProps){
    keysArr.push(k);

  }
  for (let i = 0; i < keysArr.length; i++) {
    if (obj[keysArr[i]] === undefined) {
      obj[keysArr[i]] = defaultProps[keysArr[i]];
    }
  }
  return obj;
}
export {defaults}
